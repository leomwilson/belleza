#!/usr/bin/zsh

VIRTUAL_ENV_DISABLE_PROMPT=1

POWERLEVEL9K_MODE="Powerline"

POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_RPROMPT_ON_NEWLINE=false

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(dir dir_writable vcs virtualenv)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status command_execution_time context)

POWERLEVEL9K_HOME_ICON=''
POWERLEVEL9K_HOME_SUB_ICON=''
POWERLEVEL9K_FOLDER_ICON=''
POWERLEVEL9K_ETC_ICON=''

POWERLEVEL9K_DIR_HOME_BACKGROUND="63"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND="black"
POWERLEVEL9K_DIR_DEFAULT_BACKGROUND="63"
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND="black"
POWERLEVEL9K_DIR_ETC_BACKGROUND="63"
POWERLEVEL9K_DIR_ETC_FOREGROUND="black"

POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_BACKGROUND="239"
POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_FOREGROUND="124"

POWERLEVEL9K_HIDE_BRANCH_ICON=true
POWERLEVEL9K_VCS_CLEAN_BACKGROUND="92"
POWERLEVEL9K_VCS_CLEAN_FOREGROUND="black"
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND="92"
POWERLEVEL9K_VCS_MODIFIED_FOREGROUND="black"
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND="92"
POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND="black"

POWERLEVEL9K_VIRTUALENV_BACKGROUND="92"
POWERLEVEL9K_VIRTUALENV_FOREGROUND="black"

POWERLEVEL9K_STATUS_OK=false
POWERLEVEL9K_STATUS_HIDE_SIGNAME=true
POWERLEVEL9K_CARRIAGE_RETURN_ICON=""
POWERLEVEL9K_STATUS_ERROR_BACKGROUND="124"
POWERLEVEL9K_STATUS_ERROR_FOREGROUND="black"

POWERLEVEL9K_EXECUTION_TIME_ICON=""
POWERLEVEL9K_COMMAND_EXECUTION_TIME_THRESHOLD=1
POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND="92"
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND="black"

POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND="63"
POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND="black"
POWERLEVEL9K_CONTEXT_ROOT_BACKGROUND="124"
POWERLEVEL9K_CONTEXT_ROOT_FOREGROUND="black"
POWERLEVEL9K_CONTEXT_SUDO_BACKGROUND="125"
POWERLEVEL9K_CONTEXT_SUDO_FOREGROUND="black"
POWERLEVEL9K_CONTEXT_REMOTE_BACKGROUND="63"
POWERLEVEL9K_CONTEXT_REMOTE_FOREGROUND="243"
POWERLEVEL9K_CONTEXT_REMOTE_SUDO_BACKGROUND="125"
POWERLEVEL9K_CONTEXT_REMOTE_SUDO_FOREGROUND="243"

POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%F{92} %F{white}"
PS2="%F{92} %F{white}"
