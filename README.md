# belleza

A semi-minimalist theme for powerlevel[9,10]k

![Screenshot](screenshot.png)

## Installation
### Zplugin
```
zplugin ice from"gitlab"
zplugin light leomwilson/belleza
```
